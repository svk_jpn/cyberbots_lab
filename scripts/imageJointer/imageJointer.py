import pandas as pd 
import numpy as np 
import cv2
from glob import glob
from os import path

def concatImage(src_dir:str, mat = (3,3), size = (640, 480)):
    '''
    mat: 並べ方(row, col)
    '''
    elements = glob(src_dir + '/*')
    files = []
    for element in elements:
        if path.isfile(element):
            files.append(element)
    files.sort()
    image_num = mat[0] * mat[1]
    width_list = np.array([size[0] // mat[1]] * mat[1])
    width_list[:(size[0] % mat[1])] += 1
    height_list = np.array([size[1] // mat[0]] * mat[0])
    height_list[:(size[1] % mat[0])] += 1

    res_img = np.zeros((size[1], size[0], 3), dtype=np.uint8)
    cnt = 0
    for file in files:
        try:
            img = cv2.imread(file)
        except:
            continue    # 読めなかったら次
        if img is None:
            continue
        row = cnt // mat[1] 
        col = cnt % mat[1] 
        offset_x = np.sum(width_list[:col])
        offset_y = np.sum(height_list[:row])
        width = width_list[col]
        height = height_list[row]
        img = cv2.resize(img, (width, height))
        res_img[offset_y:offset_y + height, offset_x:offset_x + width] = img
        cnt+=1
    return res_img

if __name__ == '__main__':
    df = pd.read_csv('config.csv')
    for data in df.iterrows():
        src_dir = data[1]['ディレクトリ名']
        mat = (data[1]['行数'], data[1]['列数'])
        size = (data[1]['幅(pixel)'], data[1]['高さ(pixel)'])
        image = concatImage(src_dir, mat, size)
        cv2.imwrite(src_dir + '.png', image)