# 対戦環境情報


## 1. 関連ファイル

## 2. 説明

## 2.1. コミュニティ
**Discord**  
[サイバーボッツの村](https://discord.com/invite/BCvAchBwn7)  
緩やかなコミュニティ。  
対戦相手の募集や、質問、雑談などに気軽に使って欲しい。  
イベントの要望なども歓迎だ。  

**攻略Wiki**  
[サイバーボッツ攻略Wiki](https://gitlab.com/svk_jpn/cyberbots_lab/-/wikis/home)  
非常に内容の充実したWikiで、日本語ながら、海外勢でも参考にしているプレイヤーがいるほどだ。  
現在も情報の更新が続いているので、参考にしてみてほしい。  

## 2.2. 大会
EVO Japan 2024 ではサイドイベントが実施され、参加者16名でのスイスドローの大会が行われた。  
結果や動画などの詳細は、Tonamelから確認できるぞ。  
[Tonamelのページ](https://tonamel.com/competition/xhPVm)  

参加希望者からの要望があれば、今後もオフライン/オンラインともに大会を検討しているとのことだ。  

## 2.3. 対戦会

**毎週水曜定期対戦会**  
Steam版のファイコレで21:00～23:00に開催されている。  
配信は基本的には[Twitch](https://www.twitch.tv/zyl_i_zinm)で行われているが、たまに[YouTube](https://www.youtube.com/@savaki9400/streams)で行われることもある。  
参加者は日によって幅があるが、3～9名程度との事だ。  
現在一番参加しやすく盛り上がっている対戦会だ。  

**毎月第3日曜定期対戦会**  
中野TRFで14:00～17:00辺りに開催されている。  
実機のアーケード版が好みであれば、こちらもおすすめだ。  

**不定期な対戦**  
DiscordやX上での呼びかけで、突発的に小規模な対戦が行われることもある。  
時間が合わない、Steam版以外の相手を探したいなとの場合は、声をかけてみるのも良いだろう。  

ゲーマーズBAR Shout Essenceにて不定期で対戦会が開催されるようです(1～2か月に1回程度)  
https://x.com/ShoutEssence/status/1801888624788807697  

## 2.4. 対戦動画
直近の物で、現行の対戦状況を表しているもの。  
(なんかしらないけど、比較的古いa-choやミカドの動画が引用されることが多いです。  
やはり、何をやっているかより、誰がやっているかの方が観る専には重要なのかもしれませんね。)  


### 2.4.1. 実況あり
実況があると素材としては使いにくいかもしれませんが、直近での生の盛り上がりということで。  

[EVO Japan 2024 ヘリオン同キャラ](https://www.youtube.com/watch?v=l-6GJVR9zQU&t=3682s)  

開幕、2人とも画面外にいるので、空いたところに提供を表示しても良いくらいでした。  
EVO Japanの配信動画なので、枠が付いているので、枠 in 枠になって若干使いづらいかもしれません。  

[ゲイツ永久(2024/5/29 定期対戦会)](https://www.twitch.tv/videos/2158233094?t=0h18m54s)  
この試合の〆が、長めに決まっているゲイツ永久です。  

### 2.4.2. 実況なし

[ブロディア同キャラ](https://www.youtube.com/watch?v=5debiWLUR7c&t=435s)  
ネット上では人気機体?らしいが、メインの使い手が現在あまりいない。  
ビットのチャージキャンセルや、コマ投げ漏れで出る2Hのチャージキャンセルなどをふんだんに使用し、現代っぽい感じになっています。  
